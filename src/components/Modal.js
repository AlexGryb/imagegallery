import React from 'react';
import { StyleSheet, Image, View, Modal, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useSelector } from 'react-redux'

function ModalScreen() {
  const navigation = useNavigation();
  const { ModalVisibleStatus, imageuri } = useSelector(state => state);

  return (
    <Modal
      transparent={false}
      animationType={'fade'}
      visible={ModalVisibleStatus}
      onRequestClose={() => {
        this.ShowModalFunction(!ModalVisibleStatus, '');
      }}>
      <View style={styles.modelStyle}>
        <Image
          style={styles.fullImageStyle}
          source={{ uri: imageuri }}
        />
        <TouchableOpacity
          activeOpacity={0.5}
          style={styles.closeButtonStyle}
          onPress={() => {
            navigation.navigate('Images');
          }}>
          <Image
            source={{
              uri:
                'https://raw.githubusercontent.com/AboutReact/sampleresource/master/close.png',
            }}
            style={{ width: 35, height: 35 }}
          />
        </TouchableOpacity>
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  fullImageStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    width: '98%',
    resizeMode: 'contain',
  },
  modelStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.4)',
  },
  closeButtonStyle: {
    width: 25,
    height: 25,
    top: 9,
    right: 9,
    position: 'absolute',
  },
});

export default ModalScreen;
