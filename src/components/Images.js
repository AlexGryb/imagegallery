/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  FlatList,
  Image,
  TouchableOpacity,
} from 'react-native';
import { connect } from 'react-redux';
import { requestApiData, setListening, select } from '../../actions';
import { bindActionCreators } from 'redux';

class Images extends React.Component {
  componentDidMount() {
    this.props.requestApiData();
  }

  ShowModalFunction(visible, imageURL) {
    this.props.setListening(visible);
    this.props.select(imageURL);
  }

  render() {
    return (
      <View style={styles.container}>
        <Text
          style={{
            padding: 16,
            fontSize: 24,
            textAlign: 'center',
            color: 'white',
            backgroundColor: 'grey',
          }}>
          Image Gallery
          </Text>
        <FlatList
          data={this.props.data}
          renderItem={({ item }) => (
            <View style={{ flex: 1, flexDirection: 'column', margin: 1 }}>
              <TouchableOpacity
                key={item.id}
                style={{ flex: 1 }}
                onPress={() => {
                  this.props.navigation.navigate('Modal');
                  this.ShowModalFunction(true, item.src);
                }}>
                <Image
                  style={styles.image}
                  source={{
                    uri: item.src,
                  }}
                />
                <Text style={styles.textName}>{item.name}</Text>
                <Text style={styles.text}>{item.description}</Text>
              </TouchableOpacity>
            </View>
          )}
          numColumns={3}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    height: 120,
    width: '100%',
  },
  text: {
    padding: 5,
    fontSize: 15,
    textAlign: 'center',
    color: 'black',
  },
  textName: {
    padding: 5,
    fontSize: 15,
    textAlign: 'center',
    color: 'black',
    fontWeight: 'bold',
  },
});

const mapStateToProps = state => {
  return {
    ModalVisibleStatus: state.ModalVisibleStatus,
    imageuri: state.imageuri,
    data: state.data,
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators({ requestApiData, setListening, select }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Images);
