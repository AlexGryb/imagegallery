import { call, put, takeLatest } from 'redux-saga/effects';
import { REQUEST_API_DATA, receiveApiData } from './actions';

const fetchData = async () => {
  try {
    const response = await fetch('https://api.unsplash.com/photos/?client_id=EFMs_EEl6eA_ZxrgsFlRtFl56skkoiC8nynOqRCTl9s');
    const items = await response.json();
    const arr = new Array();
    const data = arr.concat(items.map(value => ({ id: value.id, src: value.urls.full, name: value.user.name, description: value.alt_description })));
    return data;
  } catch (e) {
    console.log(e);
  }
};

function* getApiData(action) {
  try {
    const data = yield call(fetchData);
    yield put(receiveApiData(data));
  } catch (e) {
    console.log(e);
  }
}

export default function* mySaga() {
  yield takeLatest(REQUEST_API_DATA, getApiData);
}
