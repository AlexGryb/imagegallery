import { RECEIVE_API_DATA } from './actions';

const initialState = {
  imageuri: '',
  ModalVisibleStatus: false,
  data: {},
}

export default (state = initialState, action) => {
  switch (action.type) {
    case 'SELECT_IMAGE': {
      return {
        ...state,
        imageuri: action.imageuri,
      };
    }
    case 'OPEN_MODAL': {
      return {
        ...state,
        ModalVisibleStatus: true,
      };
    }
    case 'CLOSE_MODAL': {
      return {
        ...state,
        ModalVisibleStatus: false,
      };
    }
    case RECEIVE_API_DATA:
      return {
        data: action.data,
      }
    default: {
      return state;
    }
  }
};
