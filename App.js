/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import ModalScreen from './src/components/Modal';
import Images from './src/components/Images';

const Stack = createStackNavigator();

class App extends React.Component {

  render() {
    return (
      <>
        <NavigationContainer>
          <Stack.Navigator>
            <Stack.Screen
              name='Images'
              component={Images}
            />
            <Stack.Screen
              name="Modal"
              component={ModalScreen}
            />
          </Stack.Navigator>
        </NavigationContainer>
      </>
    );
  }
};

export default App;
