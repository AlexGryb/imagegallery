export const REQUEST_API_DATA = 'REQUEST_API_DATA';
export const RECEIVE_API_DATA = 'RECEIVE_API_DATA';

const setListening = value => {
  return {
    type: value ? 'OPEN_MODAL' : 'CLOSE_MODAL'
  };
};
export { setListening };

const select = imageuri => {
  return {
    type: 'SELECT_IMAGE',
    imageuri
  }
}
export { select };

export const requestApiData = () => ({ type: REQUEST_API_DATA });
export const receiveApiData = data => ({ type: RECEIVE_API_DATA, data });